
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:natural/model/introitem.dart';
const String logo = "assets/logo.png";

final List<IntroItem> introItems = [
  IntroItem(image: "assets/logo.png",title: "Load Fund", subtitle: "Load funds in your khalti from your Bank account"),
  IntroItem(image: "assets/logo.png",title: "Pay on the go", subtitle: "Recharge, make gene service payment bills and much more"),
  IntroItem(image: "assets/logo.png",title: "Fund transfer", subtitle: "Request send money to your friends."),
];