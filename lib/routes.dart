import 'package:flutter/material.dart';
import 'pages/app.dart';
import 'pages/intro.dart';
import 'pages/login.dart';
import 'pages/register.dart';
import 'pages/otp.dart';
import 'pages/home.dart';
import 'pages/list.dart';
import 'pages/splash.dart';
import 'pages/intro.dart';
import 'pages/details.dart';
import 'pages/search.dart';
import 'pages/pemesanan.dart';


class Routes {
  final routes = <String, WidgetBuilder>{
    '/app': (BuildContext context) => new App(),
    '/open': (BuildContext context) => new IntroPage(),
    '/login': (BuildContext context) => new Login(),
    '/register': (BuildContext context) => new Register(),
    '/otp': (BuildContext context) => new Otp(),
    '/list': (BuildContext context) => new ListData(),
    '/IntroPage': (BuildContext context) => new IntroPage(),
    '/detail': (BuildContext context) => new Details(),
    '/search': (BuildContext context) => new SearchScreen(),
    '/pemesanan': (BuildContext context) => new Pemesanan()
  };

  Routes () {
    runApp(new MaterialApp(
      title: 'Natural Tourism',
      routes: routes,
      home: new SplashScreen(),
    ));
  }
}