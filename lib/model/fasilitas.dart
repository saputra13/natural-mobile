class FasilitasModel {
  final String nama;
  final String icon;
  FasilitasModel({this.nama, this.icon});

  factory FasilitasModel.fromJson(Map<String, dynamic> json) {
    return FasilitasModel(
      nama: json['fasilitas'],
      icon: json['icon'],
    );
  }
}