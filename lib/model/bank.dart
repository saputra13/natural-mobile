class BankModel {
  final String bank_id;
  final String nama;
  final String icon;
  final String rek;
  final String code;
  final String owner;
  BankModel({this.bank_id,this.nama, this.icon, this.rek, this.owner,this.code});

  factory BankModel.fromJson(Map<String, dynamic> json) {
    return BankModel(
      bank_id: json['bank_id'],
      nama: json['nama'],
      icon: json['icon'],
      rek: json['rek'],
      owner: json['owner'],
      code: json['code'],
    );
  }
}