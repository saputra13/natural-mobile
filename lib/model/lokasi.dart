class LokasiModel {
  final String lokasi_id;
  final String nama;
  final int harga;
  final String foto;
  final String alamat;
  final String desk;
  final String lat;
  final String lng;
  final String kebijakan;
  LokasiModel({this.lokasi_id, this.nama, this.harga, this.foto, this.alamat, this.desk, this.lat, this.lng, this.kebijakan});

  factory LokasiModel.fromJson(Map<String, dynamic> json) {
    return LokasiModel(
      lokasi_id: json['lokasi_id'],
      nama: json['nama'],
      harga: json['harga'],
      foto: json['foto'],
      alamat: json['alamat'],
      desk: json['desk'],
      lat: json['lat'],
      lng: json['lng'],
      kebijakan: json['kebijakan'],
    );
  }
}