class UserModel {
  final String id;
  final String name;
  final int email;
  final String foto;
  UserModel({this.id, this.name, this.email, this.foto});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      foto: json['foto'],
    );
  }
}