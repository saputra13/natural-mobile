class LokasiFotolModel {
  final String foto;
  final String judul;
  LokasiFotolModel({this.foto, this.judul});

  factory LokasiFotolModel.fromJson(Map<String, dynamic> json) {
    return LokasiFotolModel(
      foto: json['foto'],
      judul: json['judul'],
    );
  }
}