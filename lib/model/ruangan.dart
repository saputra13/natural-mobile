import 'fasilitas.dart';
class RuanganModel {
  final String ruangan_id;
  final String nama;
  final int harga;
  final String foto;
  final String ranjang;
  final String ukuran;
  final String tamu;
  final int sisa;
  final List<FasilitasModel> fasilitas;
  RuanganModel({this.ruangan_id, this.nama, this.harga, this.foto, this.ranjang, this.ukuran, this.tamu, this.fasilitas, this.sisa});

  factory RuanganModel.fromJson(Map<String, dynamic> json) {
     var fasilitasx = json['fasilitasList'] as List;
    List<FasilitasModel> fasilitasList = fasilitasx.map((i) => FasilitasModel.fromJson(i)).toList();
    return RuanganModel(
      ruangan_id: json['ruangan_id'],
      nama: json['nama'],
      harga: json['harga'],
      foto: json['foto'],
      ranjang: json['ranjang'],
      ukuran: json['ukuran'],
      tamu: json['tamu'],
      sisa: json['sisa'],
      fasilitas: fasilitasList,
    );
  }
}