class BookingModel {
  final String booking_id;
  final String booking_code;
  final int grand_total;
  final String status;
  final String lokasi;
  final String ruangan;
  final String update;
  final String alamat;
  final String checkin;
  final String checkout;
  final String nama;
  BookingModel({this.booking_id, this.booking_code, this.grand_total, this.status, this.lokasi, this.ruangan, this.update, this.alamat, this.checkin, this.checkout, this.nama});

  factory BookingModel.fromJson(Map<String, dynamic> json) {
    return BookingModel(
      booking_id: json['booking_id'],
      booking_code: json['booking_code'],
      grand_total: json['grand_total'],
      status: json['status'],
      lokasi: json['ruangan']['lokasi']['nama'],
      ruangan: json['ruangan']['nama'],
      update: json['updated_at'],
      alamat: json['ruangan']['lokasi']['alamat'],
      checkin: json['checkin'],
      checkout: json['checkout'],
      nama: json['user']['name'],
    );
  }
}