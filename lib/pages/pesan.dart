import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:natural/pages/review.dart';
import 'package:natural/pages/list.dart';
import 'package:natural/pages/details.dart';
import 'package:natural/widgets/icon_badge.dart';
import 'package:natural/utils/places.dart';

class PesanScreen extends StatefulWidget {
  final String ruangan_id;
  final String tamu;
  final String ranjang;
  final String nama;
  final String lokasi;
  final String checkin;
  final String checkout;
  final int harga;

  const PesanScreen(
      {Key key,
      this.ruangan_id,
      this.tamu,
      this.ranjang,
      this.nama,
      this.lokasi, this.harga, this.checkin, this.checkout})
      : super(key: key);
  @override
  PesanScreenState createState() => PesanScreenState();
}

enum SingingCharacter { lafayette, jefferson }

class PesanScreenState extends State<PesanScreen> {
  SingingCharacter _character = SingingCharacter.lafayette;
  final TextEditingController _catatan = TextEditingController();

  @override
  void initState() {
    super.initState();
    print(widget.ruangan_id);
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Isi Data', style: TextStyle(color: Colors.white)),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0.0,
        ),
        body: Column(
          children: <Widget>[
            Container(
                color: Colors.green,
                height: height * 0.05,
                width: width,
                padding: EdgeInsets.only(left: width * 0.4),
                child: Row(
                  children: <Widget>[
                    Text('1. Pesan',
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    Padding(
                      padding: EdgeInsets.only(left: 9, right: 9),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white.withOpacity(0.5),
                        size: 20,
                      ),
                    ),
                    Text('2. Review',
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                            fontSize: 18)),
                  ],
                )),
            Expanded(
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        height: height * 0.07,
                        width: width,
                        padding: EdgeInsets.all(15),
                        color: Colors.grey.shade300,
                        child: Text('Pemesanan Anda',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600)),
                      ),
                      Container(
                        width: width,
                        height: height * 0.09,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(widget.lokasi,
                                style: TextStyle(
                                    color: Colors.black87, fontSize: 18)),
                            Text('DETAIL',
                                style: TextStyle(
                                    color: Colors.orange,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        height: height * 0.26,
                        color: Colors.white,
                        width: width,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 45),
                                      child: Text('Check-in',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.checkin,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                  Text('14:00',
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 35),
                                      child: Text('Check-out',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.checkout,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                  Text('12:00',
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 60),
                                      child: Text('Kamar',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.nama + ' (x1)',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 20),
                                      child: Text('Tipe Ranjang',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.ranjang,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 68),
                                      child: Text('Tamu',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.tamu + ' Tamu/Kamar',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        color: Colors.orangeAccent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.info,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 13),
                                  child: Text('Tidak Bisa Refund',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                )
                              ],
                            ),
                            Text('DETAIL',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      Container(
                        height: height * 0.07,
                        width: width,
                        padding: EdgeInsets.all(15),
                        color: Colors.grey.shade300,
                        child: Text('Data Pemesan (Untuk E-tiket/Voucher)',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600)),
                      ),
                      Container(
                        width: width,
                        height: height * 0.09,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(widget.nama,
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 18)),
                                Text(
                                    'Anggi Rima Saputra/anggisptr110@gmail.com',
                                    style: TextStyle(
                                        color: Colors.black26, fontSize: 14)),
                              ],
                            ),
                            // Text('UBAH',
                            //     style: TextStyle(
                            //         color: Colors.orange,
                            //         fontSize: 18,
                            //         fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Radio(
                                    value: SingingCharacter.lafayette,
                                    groupValue: _character,
                                    onChanged: (SingingCharacter value) {
                                      setState(() {
                                        _character = value;
                                      });
                                    }),
                                Text(
                                  'Saya pesan untuk pribadi',
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                    value: SingingCharacter.jefferson,
                                    groupValue: _character,
                                    onChanged: (SingingCharacter value) {
                                      setState(() {
                                        _character = value;
                                      });
                                    }),
                                Text(
                                  'Saya pesan untuk orang lain',
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(color: Colors.grey.shade300, height: 20),
                      Container(
                        width: width,
                        height: height * 0.2,
                        padding: EdgeInsets.all(15),
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Tambah Permintaan Khusus',
                                style: TextStyle(
                                    color: Colors.black87, fontSize: 18)),
                            Container(
                              height: height * 0.11,
                              width: width,
                              padding:
                                  EdgeInsets.only(left: 3, right: 3, bottom: 3),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.green,
                                  width: 1.0,
                                ),
                              ),
                              child: TextField(
                                controller: _catatan,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:
                                        'Masukan apa yang anda inginkan...'),
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                              ),
                            )
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey, height: 20, indent: 2),
                      Container(
                        padding: EdgeInsets.all(15),
                        color: Colors.white,
                        child: ButtonTheme(
                          minWidth: width * 0.3,
                          height: 40.0,
                          child: FlatButton(
                            color: Colors.red,
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) {
                                    return ReviewScreen(
                                      ruangan_id: widget.ruangan_id,
                                      tamu: widget.tamu,
                                      ranjang: widget.ranjang,
                                      nama: widget.nama,
                                      lokasi: widget.lokasi,
                                      catatan: _catatan.text,
                                      harga: widget.harga,
                                       checkin: widget.checkin,
                                              checkout: widget.checkout,
                                    );
                                  },
                                ),
                              );
                            },
                            child: Text(
                              'Lanjutkan',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                  fontFamily: 'Poppins'),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
