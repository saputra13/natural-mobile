import 'package:flutter/material.dart';
import 'package:natural/pages/inbox.dart';

class InboxDetailScreen extends StatelessWidget {
  void onButtonPressed(BuildContext context) {}

  void onBackwardArrowPressed(BuildContext context) => Navigator.pop(
      context, MaterialPageRoute(builder: (context) => Inbox()));

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Daily Promotion",
          style: TextStyle(
            color: Color.fromARGB(255, 25, 190, 25),
            fontSize: 14,
            fontFamily: "Poppins",
          ),
        ),
        leading: IconButton(
          onPressed: () => this.onBackwardArrowPressed(context),
          icon: Image.asset(
            "assets/images/backward-arrow.png",
          ),
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              left: 0,
              top: 0,
              right: 0,
              bottom: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: 82,
                    child: Column(
                      children: [
                        Container(
                          
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 25, 190, 25),
                          ),
                          width: width,
                          height: height * 0.2-62,
                          child: Image.asset(
                            "assets/images/image-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: width,
                    height: height - 273,
                    margin: EdgeInsets.only(left: 0, top: 20),
                  
                    padding: EdgeInsets.all(20),
                    child: ListView(children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "OnCall Daily Promotion For You!!",
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 22,
                                fontFamily: "Poppins",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Opacity(
                              opacity: 0.6,
                              child: Text(
                                "8/6/2019  15:55 PM",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 0, 0),
                                  fontSize: 10,
                                  fontFamily: "Poppins",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 25, 190, 25),
                            ),
                            width: width,
                            child: Image.asset(
                              "assets/images/image-2.png",
                              fit: BoxFit.none,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: 315,
                              margin: EdgeInsets.only(top: 20),
                              child: Text(
                                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit.",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 0, 0),
                                  fontSize: 10,
                                  fontFamily: "Poppins",
                                ),
                                textAlign: TextAlign.justify,
                              ),
                            ),
                          ),
                        ],
                      )
                    ]),
                  ),
                  Spacer(),
                  Container(
                    height: 67,
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 255, 255, 255),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(41, 0, 0, 0),
                          offset: Offset(0, -3),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 278,
                          height: 48,
                          child: FlatButton(
                            onPressed: () => this.onButtonPressed(context),
                            color: Color.fromARGB(255, 27, 211, 27),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            textColor: Color.fromARGB(255, 255, 255, 255),
                            padding: EdgeInsets.all(0),
                            child: Text(
                              "CLAIM",
                              style: TextStyle(
                                fontSize: 10,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
