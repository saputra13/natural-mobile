import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:natural/pages/otp.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Phone extends StatefulWidget {
  final String name;
  final String email;
  final String password;
  final String passwordconfirmation;

  Phone(
      {Key key,
      this.name,
      this.email,
      this.password,
      this.passwordconfirmation})
      : super(key: key);

  _PhoneState createState() => _PhoneState();
}

class _PhoneState extends State<Phone> {
  final phone = TextEditingController();
  int random;
  SharedPreferences sharedPreferences;
  void _random() async{
    var randomizer = new Random(); // can get a seed as a parameter
    var num = randomizer.nextInt(1000000);
    print(num);
    setState(() {
      random = num;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("otp",random.toString());
    sharedPreferences.commit();
  }

  Future<Token> verifyPhone() async {
    _random();
    final response = await http.post('http://167.71.197.116/api/otp',
        body: {'phone': phone.text, 'code': random.toString()});
    print(response.body);
    if (response.body == 'Success') {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Otp(
                  phone: phone.text,
                  name: widget.name,
                  email: widget.email,
                  password: widget.password,
                  passwordconfirmation: widget.passwordconfirmation)));
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf, terjadi gangguan. silahkan ulangi kembali",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF008f00),
      body: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(
            top: 10, left: 16.0, right: 16.0, bottom: 10.0),
        child: Stack(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 70),
              padding:
                  const EdgeInsets.only(top: 80.0, left: 16.0, right: 16.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: Colors.white),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Text(
                      "One Time Password",
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Text(
                        "Kami akan mengirimkan Password daftar via SMS. jangan diberikan kepada orang yang tidak berhak.",
                        textAlign: TextAlign.center),
                    const SizedBox(height: 20.0),
                    TextField(
                      controller: phone,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(hintText: "Nomor Telepon"),
                    ),
                    const SizedBox(height: 20.0),
                    SizedBox(
                        width: double.infinity,
                        child: RaisedButton(
                          color: Color(0xFF008f00),
                          textColor: Colors.white,
                          child: Text("MASUK".toUpperCase()),
                          onPressed: () async {
                            verifyPhone();
                          },
                        )),
                    const SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                        const SizedBox(width: 10.0),
                        Text(
                          "Bermasalah?",
                          style: TextStyle(fontSize: 12.0),
                        ),
                        const SizedBox(width: 10.0),
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    GestureDetector(
                      child: Text(
                        "Kontak Kami".toUpperCase(),
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                      onTap: () {},
                    ),
                    const SizedBox(height: 20.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
