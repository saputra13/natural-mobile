import 'package:flutter/material.dart';
import 'package:natural/pages/room_search.dart';
import 'package:natural/pages/app.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class SearchScreen extends StatefulWidget {
  @override
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  final TextEditingController checkinController = new TextEditingController();
  final TextEditingController checkoutController = new TextEditingController();
  final TextEditingController tamuController = new TextEditingController();
  List data = List(); //edited line
  final String url = "http://naturatourism.com/api/lokasi";
  String _mySelection;
  Future<String> getSWData() async {
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);
    setState(() {
      data = resBody;
    });
  }

  DateTime selectedDate = DateTime.now();
  @override
  void initState() {
    super.initState();
    this.getSWData();
    checkinController.text =
        new DateFormat('yyyy-MM-dd').format(selectedDate).toString();
  }

  Future<Null> _selectDate(BuildContext context, String type) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        if (type == 'checkin') {
          checkinController.text =
              new DateFormat('yyyy-MM-dd').format(picked).toString();
        } else {
          checkoutController.text =
              new DateFormat('yyyy-MM-dd').format(picked).toString();
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return App();
              },
            ),
          ),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: const Text('Pencarian',
              style: TextStyle(color: Colors.white)),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return App();
                  },
                ),
              );
            },
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              width: width,
              color: Colors.white,
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Container(
                    width: width,
                    child: DropdownButton(
                      isExpanded: true,
                      hint: Text('Pilih Lokasi'),
                      items: data.map((item) {
                        return new DropdownMenuItem(
                          child: new Text(item['nama']),
                          value: item['lokasi_id'].toString(),
                        );
                      }).toList(),
                      onChanged: (newVal) {
                        setState(() {
                          _mySelection = newVal;
                        });
                      },
                      value: _mySelection,
                    ),
                  ),
                  new SizedBox(height: 12.0),
                  Row(
                    children: <Widget>[
                      Container(
                        width: width * 0.5,
                        child: TextField(
                          controller: checkinController,
                          onTap: () {
                            _selectDate(context, 'checkin');
                          },
                          decoration: InputDecoration(
                            icon: Icon(Icons.calendar_today),
                            labelText: 'Checkin',
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        width: width * 0.38,
                        child: TextField(
                          controller: checkoutController,
                          onTap: () {
                            _selectDate(context, 'checkout');
                          },
                          decoration: InputDecoration(
                            labelText: 'Checkout',
                          ),
                        ),
                      ),
                      new SizedBox(height: 12.0),
                    ],
                  ),
                  Container(
                    child: TextField(
                      controller: tamuController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        icon: Icon(Icons.people),
                        labelText: 'Jumlah Tamu',
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: ButtonTheme(
                      minWidth: width,
                      height: 50.0,
                      child: FlatButton(
                        color: Colors.red,
                        onPressed: () {
                          if (_mySelection == null || checkinController.text == null || checkoutController.text == null ||  tamuController.text == null ) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    elevation: 0.0,
                                    backgroundColor: Colors.transparent,
                                    child: Container(
                                      width: 311,
                                      height: 300,
                                      padding: EdgeInsets.all(30),
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color.fromARGB(26, 0, 0, 0),
                                            offset: Offset(0, 30),
                                            blurRadius: 30,
                                          ),
                                        ],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6)),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Align(
                                            alignment: Alignment.topCenter,
                                            child: Container(
                                              width: 52,
                                              height: 52,
                                              margin: EdgeInsets.only(top: 50),
                                              child: Icon(
                                                Icons.close,
                                                color: Colors.red,
                                                size: 40,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            'Maaf, Data Pencarian anda belum lengkap, Silahkan pilih lokasi',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Poppins',
                                                fontSize: 16),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            padding: EdgeInsets.only(
                                                left: 30, right: 30),
                                            child: Container(
                                              height: 48,
                                              child: FlatButton(
                                                onPressed: () {
                                                   Navigator.pop(context);
                                                },
                                                // color: Color.fromARGB(255, 27, 211, 27),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5)),
                                                ),
                                                textColor: Color.fromARGB(
                                                    255, 255, 255, 255),
                                                padding: EdgeInsets.all(0),
                                                child: Text(
                                                  "OK",
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.blue),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                });
                          } else {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) {
                                  return RoomSearchScreen(
                                      lokasi_id: _mySelection,
                                      checkin: checkinController.text,
                                      checkout: checkoutController.text,
                                      tamu: tamuController.text);
                                },
                              ),
                            );
                          }
                        },
                        child: Text(
                          'CARI',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontFamily: 'Poppins'),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
