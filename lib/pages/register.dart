import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:natural/res/colors.dart';
import 'package:natural/res/typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:natural/pages/phone.dart';


class Register extends StatefulWidget {
  @override
  RegisterState createState() {
    return new RegisterState();
  }
}

class RegisterState extends State<Register> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _passwordconfirm = TextEditingController();
  final _name = TextEditingController();
  String _verificationId;
  String _message = '';

  bool checkValue = false;
  bool pass = false;

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
  }

  _onChanged(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences.setBool("check", checkValue);
      sharedPreferences.commit();
    });
  }

  void _showDialog() {
    String name = _name.text;
    String password = _password.text;
    String email = _email.text;
    String passwordconfirmation = _passwordconfirm.text;
    // flutter defined function
    if (pass == false) {
      _checkmatch();
    } else {
      registPost(name, password, email, passwordconfirmation);
    }
  }

  void _checkmatch() {
    //  print(_password);
    //  print(_passwordconfirm);
    if (_password.text == _passwordconfirm.text) {
      setState(() {
        pass = true;
      });
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf Password dan Konfirmasi Password tidak sama",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  Future<Token> registPost(String name, String password, String email,
      String passwordconfirmation) async {
    final response =
        await http.post('http://167.71.197.116/api/checkemail', body: {
      'email': email,
    });
    // print(response.body);
    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Phone(
                  name: name,
                  email: email,
                  password: password,
                  passwordconfirmation: passwordconfirmation)));
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf email tersebut sudah terdaftar, silahkan gunakan email lain",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF008f00),
      body: Container(
        margin: const EdgeInsets.fromLTRB(16.0, 40.0, 16.0, 16.0),
        padding: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0), color: Colors.white),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Nama Lengkap",
                style: smallText,
              ),
              TextField(
                controller: _name,
              ),
              const SizedBox(height: 20.0),
              Text(
                "Email (Optional)",
                style: smallText,
              ),
              TextField(
                controller: _email,
              ),
              const SizedBox(height: 20.0),
              Text(
                "Password",
                style: smallText,
              ),
              TextField(
                controller: _password,
                obscureText: true,
              ),
              const SizedBox(height: 20.0),
              Text(
                "Confirm Password",
                style: smallText,
              ),
              TextField(
                controller: _passwordconfirm,
                obscureText: true,
                // onChanged: (text){
                //   _checkmatch();
                // },
              ),
              const SizedBox(height: 20.0),
              Text(
                "Dengan mendaftar, kamu menyetujui  Ketentuan Layanan dan Kebijakan Privasi",
                style: smallText,
              ),
              const SizedBox(height: 10.0),
              SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    color: Color(0xFF008f00),
                    textColor: Colors.white,
                    child: Text("Daftar".toUpperCase()),
                    onPressed: _showDialog,
                  )),
              const SizedBox(height: 10.0),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Divider(
                    color: Colors.grey.shade600,
                  )),
                  const SizedBox(width: 10.0),
                  Text(
                    "Sudah punya akun?",
                    style: smallText,
                  ),
                  const SizedBox(width: 10.0),
                  Expanded(
                      child: Divider(
                    color: Colors.grey.shade600,
                  )),
                ],
              ),
              const SizedBox(height: 20.0),
              ButtonTheme(
                  minWidth: double.infinity,
                  height: 40,
                  padding: EdgeInsets.all(5),
                  child: RaisedButton(
                    child: Text('LOGIN', style: TextStyle(fontSize: 17)),
                    color: new Color(0xFF008f00),
                    elevation: 4.0,
                    splashColor: Colors.blueGrey,
                    textColor: Colors.white,
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/login');
                    },
                  )),
              const SizedBox(height: 20.0),
            ],
          ),
        ),
      ),
    );
  }

  TextField _buildTextField({bool obscureText = false}) {
    return TextField(
      obscureText: obscureText,
    );
  }
}

class Token {
  final String access_token;
  Token({this.access_token});
  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      access_token: json['access_token'],
    );
  }
}
