import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';

import 'package:natural/model/bank.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:natural/pages/konfirmasi.dart';
import 'dart:math';

class BayarScreen extends StatefulWidget {
  final String booking;
  final String lokasi;
  final String checkin;
  final String checkout;
  final int harga;
  final String kamar;
  final String booking_id;
  final int unique;

  const BayarScreen(
      {Key key,
      this.booking,
      this.lokasi,
      this.harga,
      this.checkin,
      this.kamar,
      this.checkout,
      this.booking_id,
      this.unique})
      : super(key: key);
  @override
  BayarScreenState createState() => BayarScreenState();
}

List<BankModel> _bank = [];

class BayarScreenState extends State<BayarScreen> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  
  @override
  void initState() { 
    super.initState();
    _bank.clear();
    getBank();
  }
  Future<Null> getBank() async {
    //  _searchResult.clear();
    //  _productDetails.clear();
    final response = await http.get('http://167.71.197.116/api/bank', headers: {
      HttpHeaders.acceptHeader: "application/json",
    });
    final responseJson = json.decode(response.body);
    setState(() {
      for (Map data in responseJson) {
        _bank.add(BankModel.fromJson(data));
      }
    });
  }

  Future<Null> postBank(String bank_id, String icon, String owner, String rek, String code,
      String bank) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token');
    final response =
        await http.post('http://167.71.197.116/api/payment', body: {
      "booking_id": widget.booking_id,
      "bank_id": bank_id,
    }, headers: {
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + _token
    });
    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      // print(res);
      if (res['message'] == 'berhasil') {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) {
              return KonfirmasiScreen(
                icon: icon,
                booking: widget.booking,
                bank: bank,
                rek: rek,
                owner: owner,
                kamar: widget.kamar,
                harga: widget.harga,
                code: code,
                random: widget.unique,
              );
            },
          ),
        );
      } else {
        showDialog(
            context: context,
            barrierDismissible: false,
            child: new CupertinoAlertDialog(
              content: new Text(
                "Maaf, Booking anda Gagal karena masalah jaringan. silahkan ulangi kembali",
                style: new TextStyle(fontSize: 16.0),
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text("OK"))
              ],
            ));
      }
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf, Booking anda Gagal karena masalah jaringan. silahkan ulangi kembali",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Pilih Metode Pembayaran ',
                  style: TextStyle(color: Colors.white, fontSize: 20)),
              Text('No Pesanan ' + widget.booking,
                  style: TextStyle(color: Colors.white, fontSize: 14)),
            ],
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0.0,
        ),
        body: Column(
          children: <Widget>[
            Container(
                color: Colors.green,
                height: height * 0.05,
                width: width,
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('2. Review',
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.5),
                                fontSize: 18)),
                        Padding(
                          padding: EdgeInsets.only(left: 9, right: 9),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white.withOpacity(0.5),
                            size: 20,
                          ),
                        ),
                        Text('3. Bayar',
                            style:
                                TextStyle(color: Colors.white, fontSize: 18)),
                        Padding(
                          padding: EdgeInsets.only(left: 9, right: 9),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white.withOpacity(0.5),
                            size: 20,
                          ),
                        ),
                        Text('4. Voucher',
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.5),
                                fontSize: 18)),
                      ],
                    ),
                  ],
                )),
            Expanded(
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        height: height * 0.13,
                        width: width,
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(
                                        Icons.business,
                                        size: 30,
                                        color: Colors.grey,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(widget.lokasi,
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    color: Colors.black87)),
                                            Text(
                                                widget.checkin +
                                                    " - " +
                                                    widget.checkout,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black38)),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  Text('Detail',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.orangeAccent)),
                                ],
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade100,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(0.0, 1.0))
                                  ]),
                              width: width,
                              height: 31,
                              padding: EdgeInsets.only(left: 53, top: 8),
                              child: Text(
                                  'Rp. ' + oCcy.format(widget.harga).toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black87)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 40,
                        width: width,
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Pilih Metode Pembayaran',
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold)),
                            ListView.builder(
                                primary: false,
                                // physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _bank == null ? 0 : _bank.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      postBank(
                                          _bank[index].bank_id,
                                          _bank[index].icon,
                                          _bank[index].owner,
                                          _bank[index].rek,
                                          _bank[index].code,
                                          _bank[index].nama);
                                    },
                                    child: Card(
                                        child: Container(
                                      decoration: new BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(5.0))),
                                      padding: EdgeInsets.all(10),
                                      width: width,
                                      height: 136,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text(
                                              'Bank Transfer - ' +
                                                  _bank[index].nama,
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold)),
                                          Divider(
                                            color: Colors.grey,
                                          ),
                                          Container(
                                            child: Image.network(
                                              'http://167.71.197.116/images/bank/${_bank[index].icon}',
                                              height: 80,
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                                  );
                                }),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
