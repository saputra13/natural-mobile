import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:natural/res/colors.dart';
import 'package:natural/res/constants.dart';
import 'package:natural/res/typography.dart';




import 'package:flutter/cupertino.dart';


class Login extends StatefulWidget {
  @override
  LoginState createState() {
    return new LoginState();
  }
}

class LoginState extends State<Login> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool checkValue = false;

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
  }



  _onChanged(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences.setBool("check", checkValue);
      sharedPreferences.setString("username", _usernameController.text);
      sharedPreferences.setString("password", _passwordController.text);
      sharedPreferences.commit();
    });
  }

  void _showDialog() {
    String username = _usernameController.text;
    String password = _passwordController.text;
    // flutter defined function
    loginPost(username, password);
  }

  Future<Token> loginPost(String username, String password) async {
    final response = await http.post('http://167.71.197.116/api/login',
        body: {'email': username, 'password': password});

    print(response.body);
    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      print(res);
      sharedPreferences = await SharedPreferences.getInstance();
      setState(() {
        sharedPreferences.setString("token", res['access_token']);
        sharedPreferences.commit();
      });
      Navigator.of(context).pushNamed('/app'); //2
    } else {
      // If that call was not successful, throw an error.
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "username dan password salah !",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
      // throw Exception('Failed to load post');
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF008f00),
      body: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(
            top: 10, left: 16.0, right: 16.0, bottom: 10.0),
        child: Stack(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 70),
              padding:
                  const EdgeInsets.only(top: 80.0, left: 16.0, right: 16.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: Colors.white),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.perm_contact_calendar),
                        const SizedBox(width: 10.0),
                        Expanded(
                          child: TextField(
                            controller: _usernameController,
                            decoration: InputDecoration(
                                hintText: "Email"),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.lock),
                        const SizedBox(width: 10.0),
                        Expanded(
                          child: TextField(
                            controller: _passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                hintText: "Password",
                                suffixIcon: GestureDetector(
                                  child: Icon(Icons.remove_red_eye),
                                  onTap: () {},
                                )),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30.0),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16.0),
                        width: double.infinity,
                        child: RaisedButton(
                          color: Color(0xFF008f00),
                          textColor: Colors.white,
                          child: Text("Login".toUpperCase()),
                          onPressed: _showDialog,
                        )),
                    const SizedBox(height: 20.0),
                    GestureDetector(
                      child: Text(
                        "Lupa Password".toUpperCase(),
                        style: TextStyle(
                            color: Color(0xFF008f00),
                            fontWeight: FontWeight.w600),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, 'recover');
                      },
                    ),
                    const SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                        const SizedBox(width: 10.0),
                        Text(
                          "Belum menjadi Member?",
                          style: smallText,
                        ),
                        const SizedBox(width: 10.0),
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    ButtonTheme(
                        minWidth: double.infinity,
                        height: 40,
                        padding: EdgeInsets.all(5),
                        child: RaisedButton(
                          child: Text('DAFTAR', style: TextStyle(fontSize: 17)),
                          color: new Color(0xFF008f00),
                          elevation: 4.0,
                          splashColor: Colors.blueGrey,
                          textColor: Colors.white,
                          onPressed: () {
                            Navigator.pushReplacementNamed(context, '/daftar');
                          },
                        )),
                    const SizedBox(height: 20.0),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Token {
  final String access_token;
  Token({this.access_token});
  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      access_token: json['access_token'],
    );
  }
}

class SnackBarPage {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: Text('Yay! A SnackBar!'),
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
                // Some code to undo the change!
              },
            ),
          );

          // Find the Scaffold in the Widget tree and use it to show a SnackBar!
          Scaffold.of(context).showSnackBar(snackBar);
        },
        child: Text('Show SnackBar'),
      ),
    );
  }
}
