import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';

import 'package:natural/model/bank.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:natural/pages/konfirmasi.dart';
import 'dart:math';

class VoucherScreen extends StatefulWidget {
   final String booking_id;
  final String booking_code;
  final int grand_total;
  final String status;
  final String lokasi;
  final String ruangan;
  final String update;
  final String alamat;
  final String checkin;
  final String checkout;
  final String nama;

  const VoucherScreen({Key key, this.booking_id, this.booking_code, this.grand_total, this.status, this.lokasi, this.ruangan, this.update, this.alamat, this.checkin, this.checkout, this.nama}) : super(key: key);
  @override
  VoucherScreenState createState() => VoucherScreenState();
}

List<BankModel> _bank = [];

class VoucherScreenState extends State<VoucherScreen> {
  final oCcy = new NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    super.initState();
    _bank.clear();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(widget.lokasi,
                  style: TextStyle(color: Colors.white, fontSize: 20)),
              Text('No Pesanan '+ widget.booking_code,
                  style: TextStyle(color: Colors.white, fontSize: 14)),
            ],
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0.0,
        ),
        body: Container(
          padding: EdgeInsets.only(left: 13, right: 13),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Stack(
                  children: <Widget>[
                    ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Card(
                            child: Container(
                              padding: EdgeInsets.all(13),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.hotel,
                                        size: 20,
                                        color: Colors.black45,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Detail Pesanan',
                                          style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    widget.nama,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text('Checkin',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black45)),
                                              Text(widget.checkin,
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.black87)),
                                              Text('14:00',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black87)),
                                            ]),
                                        Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.hotel,
                                                color: Colors.black54,
                                                size: 20,
                                              ),
                                              Text('1 Malam',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black87)),
                                            ]),
                                        Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Text('Checkout',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black45)),
                                              Text(widget.checkout,
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.black87)),
                                              Text('12:00',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black87)),
                                            ]),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Card(
                            child: Container(
                              padding: EdgeInsets.all(13),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.hotel,
                                        size: 20,
                                        color: Colors.black45,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'Detail Hotel',
                                          style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top:8.0),
                                    child: Text(
                                      widget.ruangan,
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.orangeAccent),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Text(
                                      widget.alamat,
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black87),
                                    ),
                                  ),
                                 
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
