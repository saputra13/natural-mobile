import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';


import 'package:flutter/material.dart';
import 'package:natural/model/lokasi_foto.dart';
import 'package:natural/utils/places.dart';
import 'package:natural/pages/room.dart';
import 'package:natural/widgets/icon_badge.dart';

class Details extends StatefulWidget {
  
  final String lokasi_id;
  const Details({Key key, this.lokasi_id}) : super(key: key);
  @override
  _DetailsState createState() => _DetailsState();
}

List<LokasiFotolModel> _foto = [];

class _DetailsState extends State<Details> {

bool _loadingInProgress = true;

  String nama,alamat,kebijakan,desk;
  int harga;
  final oCcy = new NumberFormat("#,##0", "en_US");

 

  Future<Null> getLokasi() async {
    //  _searchResult.clear();
    //  _productDetails.clear();
    final response =
        await http.get('http://167.71.197.116/api/lokasi/detail/'+widget.lokasi_id, headers: {
      HttpHeaders.acceptHeader: "application/json"
    });
    final responseJson = json.decode(response.body);
    setState(() {
      nama = responseJson['nama'];
      alamat = responseJson['alamat'];
      kebijakan = responseJson['kebijakan'];
      desk = responseJson['desk'];
      harga = responseJson['harga'];
      for (Map foto in responseJson['foto']) {
        _foto.add(LokasiFotolModel.fromJson(foto));
      }
      _dataLoaded();
    });
  }
  void _dataLoaded() {
    setState(() {
      _loadingInProgress = false;
    });
  }

  
  @override
  void initState() {
    super.initState();
    getLokasi();

    setState(() {
      _foto.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_loadingInProgress) {
      return Container(
        color: Color(0xFFFFFFFF),
        child: new Center(
          child: new CircularProgressIndicator(),
        ),
      );
    } else {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("${nama}"),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.only(left: 20),
            height: 250,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              primary: false,
              itemCount: _foto == null ? 0 : _foto.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      "http://167.71.197.116/images/lokasi/${_foto[index].foto}",
                      height: 250,
                      width: MediaQuery.of(context).size.width - 40,
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(height: 20),
          ListView(
            padding: EdgeInsets.symmetric(horizontal: 20),
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "${nama}",
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.bookmark,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.location_on,
                    size: 14,
                    color: Colors.blueGrey[300],
                  ),
                  SizedBox(width: 3),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "${alamat}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                        color: Colors.blueGrey[300],
                      ),
                      maxLines: 1,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Rp. ${oCcy.format(harga)}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                  ),
                  maxLines: 1,
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: 40),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Details",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                  maxLines: 1,
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: 10),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "${desk}",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 50,
        child: RaisedButton(
          elevation: 15,
          color: Colors.red,
          child: Text(
            "Pilih Kamar",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => new RoomScreen(lokasi_id: widget.lokasi_id, lokasi_name: "${nama}")));
          },
        ),
      ),
    );
    }
  }
}
