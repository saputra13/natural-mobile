import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:natural/pages/bayar.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReviewScreen extends StatefulWidget {
  final String ruangan_id;
  final String tamu;
  final String ranjang;
  final String nama;
  final String lokasi;
  final String catatan;
  final String checkin;
  final String checkout;
  final int harga;

  const ReviewScreen(
      {Key key,
      this.ruangan_id,
      this.tamu,
      this.ranjang,
      this.nama,
      this.lokasi,
      this.catatan,
      this.harga,
      this.checkin,
      this.checkout})
      : super(key: key);
  @override
  ReviewScreenState createState() => ReviewScreenState();
}

enum SingingCharacter { lafayette, jefferson }

class ReviewScreenState extends State<ReviewScreen> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  String booking_code;
  String booking_id;
  int random;

  // String checkin = '2019-08-14';
  // String checkout = '2019-08-15';
  @override
  void initState() {
    super.initState();
    _random();
  }

  void _random() {
    var randomizer = new Random(); // can get a seed as a parameter
    var num = randomizer.nextInt(1000);
    print(num);
    setState(() {
      random = num;
    });
  }

  Future<Null> bookingPost() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token');
    final response =
        await http.post('http://167.71.197.116/api/booking', body: {
      "ruangan_id": widget.ruangan_id,
      "checkin": widget.checkin,
      "checkout": widget.checkout,
      "total": widget.harga.toString(),
      "tax": "0",
      "discount": "0",
      "grand_total": widget.harga.toString(),
      "catatan": widget.catatan,
      "guest": "other",
      "unique": random.toString(),
      "guest_nama": "budi sudarsono"
    }, headers: {
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + _token,
    });
    print(response.body);
    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      print(res);
      if (res['message'] == 'berhasil') {
        setState(() {
          booking_code = res['booking_code'];
          booking_id = res['booking_id'];
        });
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) {
              return BayarScreen(
                  booking_id: booking_id,
                  kamar: widget.nama,
                  booking: booking_code,
                  harga: widget.harga,
                  lokasi: widget.lokasi,
                  checkin: widget.checkin,
                  unique: random,
                  checkout: widget.checkout);
            },
          ),
        );
      } else {
        showDialog(
            context: context,
            barrierDismissible: false,
            child: new CupertinoAlertDialog(
              content: new Text(
                "Maaf, Booking anda Gagal karena masalah jaringan. silahkan ulangi kembali",
                style: new TextStyle(fontSize: 16.0),
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text("OK"))
              ],
            ));
      }
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf, Booking anda Gagal karena masalah jaringan. silahkan ulangi kembali",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Isi Data', style: TextStyle(color: Colors.white)),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0.0,
        ),
        body: Column(
          children: <Widget>[
            Container(
                color: Colors.green,
                height: height * 0.05,
                width: width,
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('1. Pesan',
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                            fontSize: 18)),
                    Padding(
                      padding: EdgeInsets.only(left: 9, right: 9),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white.withOpacity(0.5),
                        size: 20,
                      ),
                    ),
                    Text('2. Review',
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    Padding(
                      padding: EdgeInsets.only(left: 9, right: 9),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white.withOpacity(0.5),
                        size: 20,
                      ),
                    ),
                    Text('3. Bayar',
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                            fontSize: 18)),
                  ],
                )),
            Expanded(
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        height: height * 0.07,
                        width: width,
                        padding: EdgeInsets.all(15),
                        color: Colors.white,
                        child: Text(
                            'Teliti kembali pemesanan Anda sebelum lanjut ke pembayaran',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 18,
                                fontWeight: FontWeight.w600)),
                      ),
                      Container(
                        width: width,
                        height: height * 0.09,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Text(widget.lokasi,
                            style:
                                TextStyle(color: Colors.black87, fontSize: 18)),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        height: height * 0.15,
                        width: width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(right: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text('Check-in',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text(widget.checkin,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                  Text('14:00',
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text('Check-out',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text(widget.checkout,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                  Text('12:00',
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ],
                              ),
                            ),
                            // Container(
                            //   padding: EdgeInsets.only(bottom: 15),
                            //   child: Column(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.start,
                            //     children: <Widget>[
                            //       Padding(
                            //           padding: EdgeInsets.only(bottom: 10),
                            //           child: Text('Durasi',
                            //               style: TextStyle(
                            //                   fontSize: 16,
                            //                   color: Colors.black26))),
                            //       Padding(
                            //           padding: EdgeInsets.only(right: 0),
                            //           child: Text('1 Malam',
                            //               style: TextStyle(
                            //                   fontSize: 16,
                            //                   color: Colors.black87))),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 45),
                                      child: Text('Kamar',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.nama + ' (x1)',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 45),
                                      child: Text('Ranjang',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.ranjang,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(right: 45),
                                      child: Text('Kapasitas',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black26))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(widget.tamu + ' Orang',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 20),
                                      child: Text(
                                          'Kebijakan Pembatalan berlaku',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600))),
                                  Padding(
                                      padding: EdgeInsets.only(right: 30),
                                      child: Text(
                                          'Pemesanan ini tidak dapat di-refund. \nWaktu yang ditampilkan sesuai dengan waktu lokal akomodasi. Tanggal inap dan tipe kamar/unit tidak dapat diubah.',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black87))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(color: Colors.grey.shade400, height: 20),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Text('x1 Kamar jenis ' + widget.nama,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600)),
                            ),
                            Text('Rp. ' + oCcy.format(widget.harga).toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Text('Pajak',
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600)),
                            ),
                            Text('Rp. 0',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Text('Total Pemesanan',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Text('Rp. ' + oCcy.format(widget.harga).toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey, height: 20, indent: 2),
                      Container(
                        padding: EdgeInsets.all(15),
                        color: Colors.white,
                        child: ButtonTheme(
                          minWidth: width * 0.3,
                          height: 40.0,
                          child: FlatButton(
                            color: Colors.red,
                            onPressed: () {
                              // Navigator.of(context).push(
                              //   MaterialPageRoute(
                              //     builder: (BuildContext context) {
                              //       return BayarScreen();
                              //     },
                              //   ),
                              // );
                              bookingPost();
                            },
                            child: Text(
                              'Konfirmasi',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                  fontFamily: 'Poppins'),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
