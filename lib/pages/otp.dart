import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:natural/res/typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Otp extends StatefulWidget {
  final String phone;
  // final String verificationId;
  final String name;
  final String email;
  final String password;
  final String passwordconfirmation;

  const Otp(
      {Key key,
      this.phone,
      this.name,
      this.email,
      this.password,
      this.passwordconfirmation})
      : super(key: key);
  @override
  OtpState createState() {
    return new OtpState();
  }
}

class OtpState extends State<Otp> {
  var timeout = const Duration(seconds: 3);
  var ms = const Duration(milliseconds: 1);

  startTimeout([int milliseconds]) {
    var duration = milliseconds == null ? timeout : ms * milliseconds;
    return new Timer(duration, handleTimeout);
  }

  void handleTimeout() {
    print('out');
  }

  String _message = '';
  String code;
  TextEditingController codecontroller = TextEditingController();
  String thisText = "";
  int pinLength = 6;
  String _verificationId;
  ScaffoldState _scaffold;
  SharedPreferences sharedPreferences;

  bool hasError = false;
  String errorMessage;
  @override
  void initState() {
    // TODO: implement initState
  }

  void confirm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    code = prefs.getString('otp');
    print(code);
    if (code == codecontroller.text) {
      registPost();
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "Maaf, kode yang anda masukan salah. silahkan ulangi kembali",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  Future<Token> registPost() async {
    final response = await http.post('http://167.71.197.116/api/signup', body: {
      'name': widget.name,
      'email': widget.email,
      'password': widget.password,
      'password_confirmation': widget.passwordconfirmation,
      'phone': widget.phone,
    });
    // print(response.body);
    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      // print();
      var token = res['access_token'];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("token", token);
      prefs.commit();
      Navigator.pushReplacementNamed(context, '/app');
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "username dan password salah !",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF008f00),
      body: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(
            top: 10, left: 16.0, right: 16.0, bottom: 10.0),
        child: Stack(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 70),
              padding:
                  const EdgeInsets.only(top: 80.0, left: 14.0, right: 14.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: Colors.white),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Text(
                      "One Time Password",
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Text(
                        "Kami akan mengirimkan Password daftar via SMS. jangan diberikan kepada orang yang tidak berhak.",
                        textAlign: TextAlign.center),
                    Container(
                      padding: EdgeInsets.only(right: 40, left: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              // _verifyPhoneNumber();
                            },
                            child: Container(
                              padding: EdgeInsets.only(top: 20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.refresh,
                                    size: 20,
                                    color: Colors.green,
                                  ),
                                  Text("Kirim Ulang",
                                      style: TextStyle(
                                        color: Colors.green,
                                      ),
                                      textAlign: TextAlign.center),
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: EdgeInsets.only(top: 20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.edit,
                                    size: 20,
                                    color: Colors.green,
                                  ),
                                  Text("Ganti Nomor",
                                      style: TextStyle(
                                        color: Colors.green,
                                      ),
                                      textAlign: TextAlign.center),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    PinCodeTextField(
                      autofocus: true,
                      controller: codecontroller,
                      hideCharacter: true,
                      highlight: true,
                      highlightColor: Colors.blue,
                      defaultBorderColor: Colors.black,
                      hasTextBorderColor: Colors.green,
                      maxLength: pinLength,
                      hasError: hasError,
                      maskCharacter: "*",
                      onTextChanged: (text) {
                        setState(() {
                          hasError = false;
                        });
                      },
                      onDone: (text) {
                        // print("DONE $text");
                        // _signInWithPhoneNumber();
                        confirm();
                      },
                      pinCodeTextFieldLayoutType:
                          PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
                      wrapAlignment: WrapAlignment.start,
                      pinBoxDecoration:
                          ProvidedPinBoxDecoration.underlinedPinBoxDecoration,
                      pinTextStyle: TextStyle(fontSize: 20.0),
                      pinTextAnimatedSwitcherTransition:
                          ProvidedPinBoxTextAnimation.scalingTransition,
                      pinTextAnimatedSwitcherDuration:
                          Duration(milliseconds: 100),
                      pinBoxWidth: 50,
                    ),
                    Visibility(
                      child: Text(
                        "Maaf pin yang anda masukan salah",
                        style: TextStyle(color: Colors.red),
                      ),
                      visible: hasError,
                    ),
                    const SizedBox(height: 20.0),
                    SizedBox(
                        width: double.infinity,
                        child: RaisedButton(
                          color: Color(0xFF008f00),
                          textColor: Colors.white,
                          child: Text("MASUK".toUpperCase()),
                          onPressed: () async {
                            // _signInWithPhoneNumber();
                            confirm();
                          },
                        )),
                    const SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                        const SizedBox(width: 10.0),
                        Text(
                          "Bermasalah?",
                          style: smallText,
                        ),
                        const SizedBox(width: 10.0),
                        Expanded(
                            child: Divider(
                          color: Colors.grey.shade600,
                        )),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    GestureDetector(
                      child: Text(
                        "Kontak Kami".toUpperCase(),
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                      onTap: () {},
                    ),
                    const SizedBox(height: 20.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Token {
  final String access_token;
  Token({this.access_token});
  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      access_token: json['access_token'],
    );
  }
}
