import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/services.dart';
import 'package:natural/pages/akun.dart';
import 'package:natural/pages/home.dart';
import 'package:natural/pages/inbox.dart';
import 'package:natural/pages/pemesanan.dart';
import 'package:flutter/material.dart';

void main() => runApp(new App());

class App extends StatefulWidget {
  final int xstate;

  const App({Key key, this.xstate}) : super(key: key);
  @override
  AppState createState() {
    return new AppState();
  }
}

class AppState extends State<App> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  String _token;
  @override
  void initState() {
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Container(
                  width: 311,
                  height: 300,
                  padding: EdgeInsets.only(left:30, right: 30),
                  decoration: BoxDecoration(
                    color: Color.fromARGB(255, 255, 255, 255),
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromARGB(26, 0, 0, 0),
                        offset: Offset(0, 30),
                        blurRadius: 30,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 52,
                          height: 52,
                          margin: EdgeInsets.only(top: 50),
                          child: Icon(
                            Icons.notifications,
                            color: Colors.green,
                            size: 40,
                          ),
                        ),
                      ),
                      Text(
                        message['notification']['title'],
                        textAlign: TextAlign.center,
                        style: TextStyle(fontFamily: 'Poppins', fontSize: 16),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 232,
                          margin: EdgeInsets.only(top: 2),
                          child: Opacity(
                            opacity: 1,
                            child: Text(
                             message['notification']['body'],
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 14,
                                fontFamily: "Poppins",
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Container(
                          height: 48,
                          child: FlatButton(
                            onPressed: () {
                               Navigator.pop(context);
                            },
                            // color: Color.fromARGB(255, 27, 211, 27),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            textColor: Color.fromARGB(255, 255, 255, 255),
                            padding: EdgeInsets.all(0),
                            child: Text(
                              "OK",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
    _fcm.getToken().then((token) {
      print(token);
      postToken(token);
    });
  }

  Future<Null> postToken(String _token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token_login = prefs.getString('token');
    final response =
        await http.post('http://167.71.197.116/api/token', headers: {
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + _token_login
    }, body: {
      'token': _token,
    });
    // final responseJson = json.decode(response.body);
    print(response.body);
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                title: new Text('Apakah Kamu yakin?'),
                content: new Text('Kamu akan keluar dari aplikasi'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: new Text('No'),
                  ),
                  new FlatButton(
                    onPressed: () {
                      SystemNavigator.pop();
                    },
                    child: new Text('Yes'),
                  ),
                ],
              ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: new MaterialApp(
        title: 'Natural Tourism',
        theme: new ThemeData(
          primarySwatch: Colors.green,
        ),
        home: new MyHomePage(xstate: widget.xstate),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final int xstate;

  const MyHomePage({Key key, this.xstate}) : super(key: key);
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  final _layoutPage = [Home(), Pemesanan(), Inbox(), Akun()];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.xstate != null) {
      setState(() {
        _selectedIndex = widget.xstate;
      });
    }
  }

  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                title: new Text('Are you sure?'),
                content: new Text('Do you want to exit an App'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: new Text('No'),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Yes'),
                  ),
                ],
              ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: _layoutPage.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Awal')),
            BottomNavigationBarItem(
                icon: Icon(Icons.view_agenda), title: Text('Pemesanan')),
            BottomNavigationBarItem(
                icon: Icon(Icons.inbox), title: Text('Inbox')),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle), title: Text('Akun Saya')),
          ],
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          onTap: _onTabItem,
        ),
      ),
    );
  }
}
