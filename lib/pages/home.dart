import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:natural/pages/list.dart';
import 'package:natural/pages/search.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(Home());

class Home extends StatefulWidget {
  // This widget is the root of your application.

  @override
  HomeState createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> {
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Natural Tourism',
      theme: ThemeData(primarySwatch: Colors.green, fontFamily: 'Poppins'),
      home: MyHomePage(title: 'Natural Tourism'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {},
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Profile(),
          Divider(),
          MenuUtama(),
          // MenuTambahan(),
          Promo(),
        ],
      ),
    );
  }
}

class Profile extends StatefulWidget {
  @override
  ProfileState createState() {
    return new ProfileState();
  }
}

class ProfileState extends State<Profile> {
  String nama, email, foto, phone;
  bool _loadingInProgress = true;

  @override
  void initState() {
    super.initState();
    getProfile();
  }

  Future<Null> getProfile() async {
    //  _searchResult.clear();
    //  _productDetails.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token');

    final response =
        await http.get('http://167.71.197.116/api/profile', headers: {
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + _token,
    });
    final responseJson = json.decode(response.body);
    setState(() {
      nama = responseJson['name'];
      email = responseJson['email'];
      foto = responseJson['foto'];
      phone = responseJson['phone'];
      _dataLoaded();
    });
  }

  void _dataLoaded() {
    setState(() {
      _loadingInProgress = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_loadingInProgress) {
      return Container(
        color: Color(0xFFFFFFFF),
        child: new Center(
          child: new CircularProgressIndicator(),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListTile(
          leading: Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.fill, image: NetworkImage("${foto}"))),
          ),
          title: Text(
            "${nama}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          subtitle: Row(
            children: <Widget>[
              // RaisedButton.icon(
              //   icon: Icon(Icons.album),
              //   label: Text("0 Poin"),
              //   onPressed: () {},
              //   color: Colors.grey[200],
              //   elevation: 0.0,
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(8.0)),
              // ),
              // Padding(
              //   padding: EdgeInsets.all(8.0),
              // ),
              // RaisedButton(
              //   child: Text("Saldo"),
              //   onPressed: () {},
              //   color: Colors.grey[200],
              //   elevation: 0.0,
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(8.0)),
              // )
            ],
          ),
        ),
      );
    }
  }
}

class MenuUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 4,
      children: menuUtamaItem,
    );
  }
}

List<MenuUtamaItems> menuUtamaItem = [
  MenuUtamaItems(
    title: "Hotel",
    icon: Icons.hotel,
    colorBox: Colors.blue,
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Resort",
    icon: Icons.business,
    colorBox: Colors.blue[900],
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Outdoor Activity",
    icon: Icons.pool,
    colorBox: Colors.purple,
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Hiking",
    icon: Icons.terrain,
    colorBox: Colors.green[300],
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Oneday Trip",
    icon: Icons.directions_run,
    colorBox: Colors.orange,
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Gathering",
    icon: Icons.group_work,
    colorBox: Colors.orange[300],
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
  MenuUtamaItems(
    title: "Daftar Lokasi",
    icon: Icons.list,
    colorBox: Colors.green,
    colorIcon: Colors.white,
    route: MaterialPageRoute(
        builder: (context) => ListData(
              checkin: '2019-09-09',
              checkout: '2019-09-09',
              tamu: '2',
            )),
  ),
  MenuUtamaItems(
    title: "Daftar Agent",
    icon: Icons.assignment_ind,
    colorBox: Colors.blue[300],
    colorIcon: Colors.white,
    route: MaterialPageRoute(builder: (context) => SearchScreen()),
  ),
];

class MenuUtamaItems extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color colorBox, colorIcon;
  final Route route;

  MenuUtamaItems(
      {this.title, this.icon, this.colorBox, this.colorIcon, this.route});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            // Navigator.of(context, rootNavigator: true).pushReplacement(
            //     MaterialPageRoute(builder: (context) => new SearchScreen()));

            Navigator.pushReplacement(context, route);
          },
          child: Container(
              width: 50.0,
              height: 50.0,
              decoration: BoxDecoration(
                color: colorBox,
                shape: BoxShape.circle,
              ),
              child: Icon(
                icon,
                color: colorIcon,
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 12.0,
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}

class Promo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(
            'Promo Saat Ini',
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22.0),
          ),
          trailing: IconButton(
            icon: Icon(Icons.keyboard_arrow_right),
            onPressed: () {},
          ),
        ),
        Container(
          width: double.infinity,
          height: 156.0,
          padding: const EdgeInsets.only(left: 8.0),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.blue,
                        Colors.blue[800],
                      ]),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                // padding: EdgeInsets.all(8.0),
                margin: EdgeInsets.only(left: 8.0),
                height: 150.0,
                width: 100.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.red[300],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.elliptical(20.0, 20.0),
                                bottomRight: Radius.elliptical(150.0, 150.0))),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 2.0, left: 5.0, right: 30.0, bottom: 30.0),
                          child: Text(
                            '%',
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                        )),
                    Expanded(
                      child: Container(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Lihat Semua \nPromo',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 18.0),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.blue,
                          Colors.blue[800],
                        ]),
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                        image: AssetImage('images/promo.jpeg'))),
                margin: EdgeInsets.only(left: 10.0),
                height: 150.0,
                width: 300.0,
                child: null,
              )
            ],
          ),
        )
      ],
    );
  }
}
