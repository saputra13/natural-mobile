import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:natural/model/lokasi.dart';
import 'package:natural/pages/details.dart';
import 'package:natural/widgets/icon_badge.dart';
import 'package:natural/utils/places.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';
import 'package:overlay_container/overlay_container.dart';

class ListData extends StatefulWidget {
  final String checkin;
  final String checkout;
  final String tamu;

  const ListData({Key key, this.checkin, this.checkout, this.tamu}) : super(key: key);
  @override
  ListState createState() => ListState();
}

List<LokasiModel> _lokasi = [];

class ListState extends State<ListData> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  final TextEditingController _searchControl = new TextEditingController();
  
  Future<Null> getLokasi() async {
    //  _searchResult.clear();
    //  _productDetails.clear();
    final response = await http.get('http://167.71.197.116/api/lokasi',
        headers: {HttpHeaders.acceptHeader: "application/json"});
    final responseJson = json.decode(response.body);
    setState(() {
      for (Map product in responseJson) {
        _lokasi.add(LokasiModel.fromJson(product));
      }
    });
  }

  @override
  void initState() {
    super.initState();

    setState(() {
      _lokasi.clear();
    });
    if (_lokasi.length == 0) {
      getLokasi();
    }
  }

  bool _dropdownShown = false;

  void _toggleDropdown() {
    setState(() {
      _dropdownShown = !_dropdownShown;
    });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.green,
          title: Container(
            width: width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Bandung',
                          style: TextStyle(color: Colors.white, fontSize: 18)),
                      Text('3 Agustus 2019, 1 Malam, 1 Kamar',
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(
                Icons.calendar_today,
                color: Colors.white,
                size: 30,
              ),
              onPressed: () {
                _toggleDropdown();
              },
            ),
            // action button
          ]),
      body: ListView(
        children: <Widget>[
          OverlayContainer(
              asWideAsParent: true,
              show: _dropdownShown,
              // Let's position this overlay to the right of the button.
              position: OverlayContainerPosition(
                // Left position.
                0,
                // Bottom position.
                0,
              ),
              // The content inside the overlay.
              child: Container(
                height: height * 0.42,
                width: width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 1,
                        spreadRadius: 2,
                        offset: Offset(0, 0))
                  ],
                ),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: TextField(
                        // controller: _usernameController,
                        decoration: InputDecoration(
                          icon: Icon(Icons.pin_drop),
                          labelText: 'Menginap di',
                        ),
                      ),
                    ),
                    new SizedBox(height: 12.0),
                    Row(
                      children: <Widget>[
                        Container(
                          width: width * 0.5,
                          child: TextField(
                            // controller: _usernameController,
                            decoration: InputDecoration(
                              icon: Icon(Icons.calendar_today),
                              labelText: 'Tanggal',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: width * 0.38,
                          child: TextField(
                            // controller: _usernameController,
                            decoration: InputDecoration(
                              labelText: 'Durasi',
                            ),
                          ),
                        ),
                        new SizedBox(height: 12.0),
                      ],
                    ),
                    Container(
                      child: TextField(
                        // controller: _usernameController,
                        decoration: InputDecoration(
                          icon: Icon(Icons.people),
                          labelText: 'Jumlah Tamu dan Kamar',
                        ),
                      ),
                    ),
                    Container(
                      child: TextField(
                        // controller: _usernameController,
                        decoration: InputDecoration(
                          icon: Icon(Icons.filter_list),
                          labelText: 'Filter (Bintang dan Harga)',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: ButtonTheme(
                        minWidth: width,
                        height: 50.0,
                        child: FlatButton(
                          color: Colors.red,
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) {
                                  return ListData();
                                },
                              ),
                            );
                          },
                          child: Text(
                            'CARI',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontFamily: 'Poppins'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )),
          Padding(
            padding: EdgeInsets.all(20),
            child: ListView.builder(
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _lokasi == null ? 0 : _lokasi.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 15.0),
                  child: InkWell(
                    child: Container(
                      height: 70,
                      child: Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Image.network(
                              "${_lokasi[index].foto}",
                              height: 70,
                              width: 70,
                              fit: BoxFit.cover,
                            ),
                          ),
                          SizedBox(width: 15),
                          Container(
                            height: 80,
                            width: MediaQuery.of(context).size.width - 130,
                            child: ListView(
                              primary: false,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "${_lokasi[index].nama}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 14,
                                    ),
                                    maxLines: 2,
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                SizedBox(height: 3),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      size: 13,
                                      color: Colors.blueGrey[300],
                                    ),
                                    SizedBox(width: 3),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "${_lokasi[index].alamat}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 13,
                                          color: Colors.blueGrey[300],
                                        ),
                                        maxLines: 1,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Rp. ${oCcy.format(_lokasi[index].harga)}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new Details(
                                  lokasi_id: _lokasi[index].lokasi_id)));
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
