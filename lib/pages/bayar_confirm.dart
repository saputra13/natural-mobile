import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:natural/pages/pemesanan.dart';
import 'package:natural/pages/app.dart';


class BayarKonfirmasiScreen extends StatefulWidget {
  final String booking;
  final int harga;
  final String bank;
  final String rek;
  final String owner;
  final String kamar;
  final String icon;
  final int random;
  final String code;

  const BayarKonfirmasiScreen(
      {Key key,
      this.booking,
      this.harga,
      this.bank,
      this.rek,
      this.owner,
      this.kamar,
      this.icon,
      this.random,
      this.code})
      : super(key: key);

  @override
  KonfirmasiScreenState createState() => KonfirmasiScreenState();
}

class KonfirmasiScreenState extends State<BayarKonfirmasiScreen> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  @override
  void initState() {
    // TODO: implement initState
    print(widget.harga);
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          backgroundColor: Color(0xFF187000),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Pilih Metode Pembayaran ',
                  style: TextStyle(color: Colors.white, fontSize: 20)),
              Text('No Pesanan #' + widget.booking,
                  style: TextStyle(color: Colors.white, fontSize: 14)),
            ],
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0.0,
        ),
        body: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    color: Color(0xFF187000),
                    height: height * 0.05,
                    width: width,
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('2. Review',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.5),
                                    fontSize: 18)),
                            Padding(
                              padding: EdgeInsets.only(left: 9, right: 9),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white.withOpacity(0.5),
                                size: 20,
                              ),
                            ),
                            Text('3. Bayar',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18)),
                            Padding(
                              padding: EdgeInsets.only(left: 9, right: 9),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white.withOpacity(0.5),
                                size: 20,
                              ),
                            ),
                            Text('4. Voucher',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.5),
                                    fontSize: 18)),
                          ],
                        ),
                      ],
                    )),
              ],
            ),
            Expanded(
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(15),
                              child: Text('1. Selesaikan Pembayaran Sebelum',
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Container(
                              height: height * 0.13,
                              width: width,
                              color: Colors.white,
                              padding: EdgeInsets.all(20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.access_time,
                                    size: 30,
                                    color: Colors.grey,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          '10.45 AM',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(fontSize: 18),
                                        ),
                                        Text(
                                          'Selesaikan pembayaran dalam 45 Menit',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black54),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        margin: EdgeInsets.only(top: 10),
                        child: Text('2. Mohon Transfer ke',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.atm,
                                    color: Colors.black38,
                                    size: 45,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('Kode Bank',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.black38,
                                                fontWeight: FontWeight.w600)),
                                        Text(widget.code,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 53.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Nomor Rekening',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black38,
                                            fontWeight: FontWeight.w600)),
                                    Text(widget.rek,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Clipboard.setData(
                                    new ClipboardData(text: widget.rek));
                              },
                              child: Text('SALIN',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 53.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Nama Pemilik Rekening',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black38,
                                            fontWeight: FontWeight.w600)),
                                    Text(widget.owner,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                            Image.network(
                              "http://167.71.197.116/images/bank/" +
                                  widget.icon,
                              height: 40,
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.5,
                          )),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 53.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Jumlah Total',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black38,
                                            fontWeight: FontWeight.w600)),
                                    Text(
                                        'Rp. ' +
                                            oCcy
                                                .format(widget.harga +
                                                    widget.random)
                                                .toString(),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Clipboard.setData(new ClipboardData(
                                    text: (widget.harga + widget.random)
                                        .toString()));
                              },
                              child: Text('SALIN',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        margin: EdgeInsets.only(top: 10),
                        child: Text('3. Anda Sudah Membayar?',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(15),
                        width: width,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top:8.0, bottom: 8),
                              child: Text(
                                'Setelah pembayaran Anda dikonfirmasi, kami akan mengirim bukti pembelian dan voucher hotel ke alamat email anda',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black87,
                                ),
                              ),
                            ),
                            ButtonTheme(
                              minWidth: width * 0.3,
                              height: 50.0,
                              child: FlatButton(
                                color: Colors.grey.shade200,
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) {
                                        return App(xstate: 1);
                                      },
                                    ),
                                  );
                                  // Navigator.pushNamed(context, '/app',arguments:'3');
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Saya Sudah Bayar',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Poppins'),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
