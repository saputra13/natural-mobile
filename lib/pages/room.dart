import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:natural/pages/list.dart';
import 'package:natural/pages/pesan.dart';
import 'package:natural/pages/details.dart';
import 'package:natural/widgets/icon_badge.dart';
import 'package:natural/utils/places.dart';
import 'package:natural/model/ruangan.dart';
import 'package:natural/model/fasilitas.dart';

List<RuanganModel> _ruangan = [];

class RoomScreen extends StatefulWidget {
  final String lokasi_id;
  final String lokasi_name;

  const RoomScreen({Key key, this.lokasi_id, this.lokasi_name})
      : super(key: key);
  @override
  RoomScreenState createState() => RoomScreenState();
}

class RoomScreenState extends State<RoomScreen> {
   final oCcy = new NumberFormat("#,##0", "en_US");
  @override
  void initState() {
    super.initState();
    _ruangan.clear();
    getLokasi();
  }

  Future<Null> getLokasi() async {
    //  _searchResult.clear();
    //  _productDetails.clear();
    final response = await http.get(
        'http://167.71.197.116/api/ruangan/' + widget.lokasi_id,
        headers: {HttpHeaders.acceptHeader: "application/json"});
    final responseJson = json.decode(response.body);
    setState(() {
      for (Map data in responseJson) {
        _ruangan.add(RuanganModel.fromJson(data));
      }
      // print(json.decode(_ruangan.toString()));
      // print(jsonDecode(_ruangan.toString()));
    });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(widget.lokasi_name, style: TextStyle(color: Colors.white)),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: ListView.builder(
        primary: false,
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _ruangan == null ? 0 : _ruangan.length,
        itemBuilder: (BuildContext context, int index) {
          RuanganModel ruanganx = _ruangan[index];
          List<FasilitasModel> fasilitasx = ruanganx.fasilitas;
          // print(fasilitasx.length);
          return Container(
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.network(
                      "http://167.71.197.116/images/ruangan/${_ruangan[index].foto}",
                      height: 130,
                      width: width,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    width: width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            width: width,
                            decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                color: Colors.grey,
                                width: 0.5,
                              )),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Text(
                                    '${_ruangan[index].nama}',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.green,
                                  ),
                                )
                              ],
                            )),
                        Container(
                          width: width,
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                              color: Colors.grey,
                              width: 0.5,
                            )),
                          ),
                          padding: EdgeInsets.only(bottom: 20),
                          margin: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${_ruangan[index].tamu} Tamu/Kamar  - Ukuran ${_ruangan[index].ukuran}',
                                style: TextStyle(color: Colors.grey),
                              ),
                              Text(
                                '${_ruangan[index].ranjang}',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                              color: Colors.grey,
                              width: 0.5,
                            )),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: 150,
                                height: 100,
                                child: ListView.builder(
                                    // primary: false,
                                    physics: NeverScrollableScrollPhysics(),
                                    // shrinkWrap: true,
                                    itemCount: fasilitasx == null
                                        ? 0
                                        : fasilitasx.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Row(
                                        children: <Widget>[
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  bottom: 5, right: 10),
                                              child: Image.network('http://167.71.197.116/images/fasilitas_ruangan/${fasilitasx[index].icon}',
                                              height: 20,
                                              width: 20,
                                              ), 
                                              ),
                                          Padding(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Text(
                                              '${fasilitasx[index].nama}',
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                          )
                                        ],
                                      );
                                    }),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  // Row(
                                  //   children: <Widget>[
                                  //     Padding(
                                  //       padding: EdgeInsets.only(bottom: 5),
                                  //       child: Text(
                                  //         'Rp. 2.000.000',
                                  //         style: TextStyle(
                                  //             color: Colors.grey,
                                  //             decoration:
                                  //                 TextDecoration.lineThrough,
                                  //             fontSize: 20),
                                  //       ),
                                  //     )
                                  //   ],
                                  // ),
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          'Rp. ${oCcy.format(_ruangan[index].harga)}',
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          'Kamar/Malam termasuk Pajak',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      // Padding(
                                      //     padding: EdgeInsets.only(bottom: 5),
                                      //     child: Icon(
                                      //       Icons.confirmation_number,
                                      //       size: 14,
                                      //       color: Colors.blue,
                                      //     )),
                                      // Padding(
                                      //   padding:
                                      //       EdgeInsets.only(bottom: 5, left: 5),
                                      //   child: Text(
                                      //     '600 Poin',
                                      //     style: TextStyle(
                                      //         color: Colors.blue,
                                      //         fontSize: 14,
                                      //         fontWeight: FontWeight.bold),
                                      //   ),
                                      // )
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text('2 Kamar tersisa',
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.red)),
                              ButtonTheme(
                                minWidth: width * 0.3,
                                height: 40.0,
                                child: FlatButton(
                                  color: Colors.red,
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (BuildContext context) {
                                          return PesanScreen(
                                            ruangan_id: "${_ruangan[index].ruangan_id}",
                                            tamu: "${_ruangan[index].tamu}",
                                            ranjang: "${_ruangan[index].ranjang}",
                                            nama: "${_ruangan[index].nama}",
                                            lokasi: widget.lokasi_name,
                                            harga: _ruangan[index].harga,
                                            );
                                        },
                                      ),
                                    );
                                  },
                                  child: Text(
                                    'Pilih',
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontFamily: 'Poppins'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
