import 'package:flutter/material.dart';

final pages = [
  new PageViewModel(const Color(0xFF548CFF), 'images/mountain.png',
      'Selamat Datang', 'Ingin liburan di alam?', 'images/plane.png'),
  new PageViewModel(
      const Color(0xFFE4534D),
      'images/world.png',
      'Kami Tempatnya',
      'Banyak pilihan liburan dengan alam yang cocok untuk keluarga, teman kerja, teman sekolah, gathering',
      'images/calendar.png'),
  new PageViewModel(
    const Color(0xFFFF682D),
    'images/home.png',
    'Yuk, di Natural Tourism',
    'Temukan liburan yang cocok untuk anda',
    'images/house.png',
  ),
];

class Page extends StatelessWidget {
  final PageViewModel viewModel;
  final double percentVisible;

  Page({
    this.viewModel,
    this.percentVisible = 1.0,
  });

  @override
  Widget build(BuildContext context) {
    return new Container(
        width: double.infinity,
        color: viewModel.color,
        child: new Opacity(
          opacity: percentVisible,
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Transform(
                  transform: new Matrix4.translationValues(
                      0.0, 50.0 * (1.0 - percentVisible), 0.0),
                  child: new Padding(
                    padding: new EdgeInsets.only(bottom: 25.0),
                    child: new Image.asset(viewModel.heroAssetPath,
                        width: 200.0, height: 200.0),
                  ),
                ),
                new Transform(
                  transform: new Matrix4.translationValues(
                      0.0, 30.0 * (1.0 - percentVisible), 0.0),
                  child: new Padding(
                    padding: new EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: new Text(
                      viewModel.title,
                      style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'FlamanteRoma',
                        fontSize: 34.0,
                      ),
                    ),
                  ),
                ),
                new Transform(
                  transform: new Matrix4.translationValues(
                      0.0, 30.0 * (1.0 - percentVisible), 0.0),
                  child: new Padding(
                    padding: new EdgeInsets.only(bottom: 75.0),
                    child: new Text(
                      viewModel.body,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'FlamanteRomaItalic',
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ),
                new Transform(
                  transform: new Matrix4.translationValues(
                      0.0, 30.0 * (1.0 - percentVisible), 0.0),
                  child: new Padding(
                    padding:
                        new EdgeInsets.only(bottom: 75.0, left: 50, right: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ButtonTheme(
                            minWidth: 100,
                            height: 40,
                            padding: EdgeInsets.all(5),
                            child: RaisedButton(
                              child:
                                  Text('LOGIN', style: TextStyle(fontSize: 17)),
                              color: new Color(0xFF008f00),
                              elevation: 4.0,
                              splashColor: Colors.blueGrey,
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/login');
                              },
                            )),
                        ButtonTheme(
                            minWidth: 100,
                            height: 40,
                            padding: EdgeInsets.all(5),
                            child: RaisedButton(
                              child: Text('DAFTAR',
                                  style: TextStyle(fontSize: 17)),
                              color: new Color(0xFF008f00),
                              elevation: 4.0,
                              splashColor: Colors.blueGrey,
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/register');
                              },
                            )),
                      ],
                    ),
                  ),
                ),
              ]),
        ));
  }
}

class PageViewModel {
  final Color color;
  final String heroAssetPath;
  final String title;
  final String body;
  final String iconAssetPath;

  PageViewModel(
    this.color,
    this.heroAssetPath,
    this.title,
    this.body,
    this.iconAssetPath,
  );
}
