import 'package:flutter/material.dart';
import 'package:natural/pages/inbox_detail.dart';

class Inbox extends StatefulWidget {
  @override
  _InboxState createState() => _InboxState();
}

class _InboxState extends State<Inbox> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "INBOX",
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontFamily: "Poppins",
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(top: 20, bottom: 0),
                child: ListView.builder(
                  itemCount: 15,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => InboxDetailScreen()));
                      },
                      child: Container(
                        height: 60,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                                height: 41,
                                margin: EdgeInsets.only(left: 50, right: 40),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 24,
                                        height: 24,
                                        margin: EdgeInsets.only(top: 7),
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              top: 0,
                                              child: Container(
                                                width: 24,
                                                height: 24,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(
                                                      255, 25, 190, 25),
                                                  border: Border.all(
                                                    color: Color.fromARGB(
                                                        255, 188, 224, 253),
                                                    width: 1,
                                                  ),
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(12)),
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                            Positioned(
                                              left: 6,
                                              top: 5,
                                              child: Container(
                                                width: 14,
                                                height: 13,
                                                child: Image.asset(
                                                  "images/mail.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        height: 40,
                                        padding: EdgeInsets.all(2),
                                        margin: EdgeInsets.only(left: 20),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Message From Natural",
                                              style: TextStyle(
                                                color:
                                                    Color.fromARGB(255, 0, 0, 0),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: "Poppins",
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                            Text(
                                              "Daily Promo",
                                              style: TextStyle(
                                                color:
                                                    Color.fromARGB(255, 0, 0, 0),
                                                fontSize: 10,
                                                fontFamily: "Poppins",
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            Spacer(),
                            Container(
                              height: 1,
                              margin: EdgeInsets.only(left: 30, right: 29),
                              child: Opacity(
                                opacity: 0.1,
                                child: Image.asset(
                                  "images/path-197.png",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
