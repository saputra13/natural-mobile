import 'package:flutter/material.dart';
import 'package:natural/pages/bill.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:natural/model/booking.dart';

class Pemesanan extends StatefulWidget {
  @override
  _PemesananState createState() => _PemesananState();
}

List<BookingModel> _booking = [];

class _PemesananState extends State<Pemesanan> {
  bool _loadingInProgress = true;
  final oCcy = new NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    super.initState();
    getLokasi();
  }

  Future<Null> getLokasi() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token');
    _booking.clear();
    final response =
        await http.get('http://167.71.197.116/api/booking/list', headers: {
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + _token,
    });
    final responseJson = json.decode(response.body);
    setState(() {
      for (Map x in responseJson) {
        _booking.add(BookingModel.fromJson(x));
      }
      _dataLoaded();
    });
  }

  void _dataLoaded() {
    setState(() {
      _loadingInProgress = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    if (_loadingInProgress) {
      return Container(
        color: Color(0xFFFFFFFF),
        child: new Center(
          child: new CircularProgressIndicator(),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          title: const Text('Riwayat Pemesanan',
              style: TextStyle(color: Colors.white, fontFamily: 'Poppins')),
          backgroundColor: Colors.green,
        ),
        body: Container(
          height: height,
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          child: ListView.builder(
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _booking == null ? 0 : _booking.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) {
                        return BillScreen(
                          booking_code: _booking[index].booking_code,
                          grand_total: _booking[index].grand_total,
                          status: _booking[index].status,
                          lokasi: _booking[index].lokasi,
                          alamat: _booking[index].alamat,
                          ruangan: _booking[index].ruangan,
                          nama: _booking[index].nama,
                          checkin: _booking[index].checkin,
                          checkout: _booking[index].checkout,
                          update: _booking[index].update,
                        );
                      },
                    ),
                  );
                },
                child: Card(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              top: 10, bottom: 10, left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('No. Pemesanan ' + _booking[index].booking_code,
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black38,
                                      fontWeight: FontWeight.bold)),
                              Text('Rp. ' + oCcy.format(_booking[index].grand_total).toString(),
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 10, right: 10),
                          color: Colors.grey.shade300,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.hotel,
                                size: 30,
                                color: Colors.black87,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(_booking[index].lokasi + " " + _booking[index].ruangan,
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 10, right: 10),
                          child: Row(
                            children: <Widget>[
                              Text(_booking[index].status,
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );
    }
  }
}
